#include "solution.h"


#include "test_utils.h"

#include <algorithm>
#include <atomic>
#include <deque>
#include <string>
#include <vector>
#include <sstream>
#include <numeric>


namespace SolutionTests {

#if defined(dump_field)
#   error "already defined"
#else
#   define dump_field(field) out << (#field) << "=" << (field)
#endif

    struct TestOpts {
        size_t n_tasks = 0;
        size_t n_runs = 0;
        std::string to_string() const {
            std::ostringstream out;
            dump_field(n_tasks) << ", ";
            dump_field(n_runs);
            return out.str();
        }
    };

    void do_test(const TestOpts& opts) {
        std::cout << "Testing with parameters: " << opts.to_string() << std::endl;

        std::vector<size_t> tasks;
        tasks.resize(opts.n_tasks);
        std::iota(tasks.begin(), tasks.end(), 0);
        
	
        for (size_t run = 0; run < opts.n_runs; ++run) {
            std::vector<std::thread::id> results;
            results.resize(opts.n_tasks);
            Barrier start{opts.n_tasks};
            std::cout << "Run " << run << " ... " << std::endl;
	    hello_world<size_t>([&results, &start](const size_t& task) {
                start.wait();
                test_assert(task < results.size() && std::thread::id() == results[task], "invalid task arguments");
                results[task] = std::this_thread::get_id(); 
            }, tasks);
            std::cout << "Done" << std::endl;
            std::sort(results.begin(), results.end());
            size_t uniq_size = std::unique(results.begin(), results.end()) - results.begin();
            test_assert(uniq_size == opts.n_tasks, "test failed: expected " << opts.n_tasks << " found: " << uniq_size);
        }

        std::cout << "Done" << std::endl;
        std::cout << "OK" << std::endl;
    }

    void run_tests(int argc, char* argv[]) {
        TestOpts opts;
        read_opts(argc, argv, opts.n_tasks, opts.n_runs);
        do_test(opts);
    }
}

int main(int argc, char* argv[]) {
    SolutionTests::run_tests(argc, argv);
}
